package com.go.photouser.service.impl;

import com.go.photouser.data.UserEntity;
import com.go.photouser.dto.UserDTO;
import com.go.photouser.mapper.UserMapper;
import com.go.photouser.repository.UserRepository;
import com.go.photouser.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDTO createUser(UserDTO userDetails) {
        userDetails.setUserid(UUID.randomUUID().toString());
        UserEntity userEntity = userMapper.userDtoToUserEntity(userDetails);
        userEntity.setEncryptedPassword(bCryptPasswordEncoder.encode(userDetails.getPassword()));
        userRepository.save(userEntity);
        return userMapper.userEntityToUserDTO(userEntity);
    }

    @Override
    public UserDTO getUserDetailsByUsername(String username) {
        UserEntity userEntity = userRepository.findByEmail(username);
        if(userEntity==null){
            throw new UsernameNotFoundException("Username is not found");
        }

        return userMapper.userEntityToUserDTO(userEntity);
    }

    @Override
    public User loadUserByUsername(String username) {
        UserEntity userEntity = userRepository.findByEmail(username);
        if(userEntity==null)
            throw new UsernameNotFoundException(username);
        return new User(userEntity.getEmail(), userEntity.getEncryptedPassword(),true, true, true, true,new ArrayList<>());
    }
}
