package com.go.photouser.mapper;

import com.go.photouser.data.UserEntity;
import com.go.photouser.dto.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
public interface UserMapper {

   UserEntity userDtoToUserEntity(UserDTO userDetails);

   UserDTO userEntityToUserDTO(UserEntity userEntity);

}
