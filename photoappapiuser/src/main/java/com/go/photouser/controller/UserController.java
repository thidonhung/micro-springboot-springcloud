package com.go.photouser.controller;


import com.go.photouser.dto.UserDTO;
import com.go.photouser.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    Environment env;

    @Autowired
    UserServiceImpl userService;

    @PostMapping
    public ResponseEntity<UserDTO> createUser(@Valid @RequestBody UserDTO userDetails){

        UserDTO userDTO = userService.createUser(userDetails);
        return ResponseEntity.status(HttpStatus.CREATED).body(userDTO);

    }

    @GetMapping("check")
    public String check(){
        return "Hi checking is done";
    }


}
