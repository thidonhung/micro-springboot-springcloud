package com.go.photouser.data;


import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "users")
public class UserEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private long id;

    @Column(name = "firstname", nullable = false)
    private String firstname;

    @Column(name="lastname", nullable = false)
    private String lastname;

    @Column(name="email" , nullable = false)
    private String email;

    @Column(name="userid" , nullable = false)
    private String userid;

    @Column(name="encryptedPassword" , nullable = false)
    private String encryptedPassword;
}
